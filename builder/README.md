
# builder

Docker image for CI builds with:

- docker-ce
- make
- wget and curl
- git
- python
- AWS CLI
- Google Cloud SDK
- kubectl
- docker-compose
- helm
- terraform 0.12.6
- ansible

