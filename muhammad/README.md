
# kubernetes-template

Required CI/CD variables:

- `KUBE_CONFIG_DATA` (encoded as base64)
- `AWS_ACCESS_KEY_ID`
- `AWS_SECRET_ACCESS_KEY`
